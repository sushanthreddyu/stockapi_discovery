package com.neural_hack.stock_api_discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class StockApiDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockApiDiscoveryApplication.class, args);
    }

}
